FROM node:5

# Create a mount point for node apps
RUN mkdir /var/opt/node

# Mount it
VOLUME ["/var/opt/node"]

# Install pm2
RUN npm install pm2 -g

# Create a dummy blank node app to initialise pm with
RUN touch /var/opt/node/index.js

# Allow parameterization of our entry point ( some may use app.js etc )
ENV NODE_ENTRY_POINT index.js


# Add an entry point to run npm install
COPY docker_entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

# Listen to the outside world
EXPOSE 8080

WORKDIR "/var/opt/node"

ENTRYPOINT [ "/entrypoint.sh" ]

CMD [ "/usr/local/bin/pm2", "start", "--watch", "--no-daemon", "index.js" ]
