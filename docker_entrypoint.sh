#!/bin/bash

set -e

# Ensure we're in the node dir
cd /var/opt/node

# Run npm install
if [ ! -d "node_modules" ]; then
  npm install
fi

# Revert file permissions to what they were initially
chown -R `stat -c %u:%g /var/opt/node` /var/opt/node

exec "$@"
