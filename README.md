Docker Node PM2 Dockerfile
==========================

This dockerfile creates a container image based on the latest `node.js`
docker image and extends it to use the node package `pm2` to provide
automatic restart and watching for file changes to the running node app.

This allows you, just having installed docker, to start running and
developing a node app based on a stable environment.

It's basic for the moment, but we will be adding test runners and other
bits pretty soon.

Build it using:

```
$ docker build -t <myroot>/<myPath> docker_node_pm2
```

Where `<myroot>/<myPath>` is the tag you want to give it.

For local development work, I recommend using something like
`local/node_pm2`

If you're the build argument in a `docker-compose` file though, you can
ignore that. `docker-compose` will manage it for you.

Software environment
--------------------

This dockerfile adds the node process manager `pm2` to the
build. ( [See docs]() )

Configuration
-------------

### volumes - mounting your node app

The container exposes a volume at `/var/opt/node` to mount your node
application directory.

Mount it using docker syntax:

```
$ docker run -v /path/on/host/to/my/nodeapp:/var/opt/node local/node_pm2
```

### Environment variables

The container has an overwritable environment variable `$NODE_ENTRY_POINT`
which defaults to `index.js`.

If your application's bootstrap file is names something different you
will need to pass that in using docker's -e argument.

```
$ docker run -e NODE_ENTRY_POINT=app.js local/node_pm2
```

### ports

The container also exposes port 8080 for listening. So if you want
to route a port from your host so you can comminicate with it ( if it's
a web app ) then you need to map the ports.

To map port 2000 on the host to 8080 in the container:

```
$ docker run -p 2000:8080 local/node_pm2
```


Example usage - putting it all together
---------------------------------------

So imagine you have a node app, and you've just cloned it's repo down
to `/home/me/development/myApp`. Let's get it up and running in the container.

First, you'll need to set up a local development configuration for your
application. That means one thing at the moment:

- Listen on port 8080.

Ok, your app may have other dependencies, they're not dealt with here. You
want to have a mysql database and a redis node? Then we're dealing with
multiple containers and you want to look at `docker-compose` and linking
containers to each other.

Enough chat, give our app at the location mentioned above we have two
things to do.

### 1. Build the image ( you only need to do this once )

What we want to do here is build the image, and store it locally with
a reusable tag ( so we can find it again ).

From the directory containing the dockerfile, run:

```
$ docker build -t local/node_pm2 docker_node_pm2
```

That will build it and add the image to the docker service. Now it's ready
for use in running things.

### 2. Run your app in a container based on the image

You can now run your app, listening on port 3001. Easy.

```
$ docker run \
  -d \ # Run it as a daemon process, you can always attach to it later
  --name my_app_container # name it, you can reference this later
  -v /home/me/development/myApp:/var/opt/node \ # Mount your app
  -p 3001:8080 \ # Link ports
  local/node_pm2 # This is the image we created
```

Next steps
----------

That's great for running an independent node app. What if you want to
do something else?

Have a look at the `docket-compose` worflows for linking multiple
containers together to create an interdependent environment of services.

You'll need to make updates to application configuration for it to work.
Rememberm this is another environment. Read up on the notes as to what
they are.
